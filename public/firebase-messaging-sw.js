importScripts('https://www.gstatic.com/firebasejs/10.6.0/firebase-app-compat.js')
importScripts('https://www.gstatic.com/firebasejs/10.6.0/firebase-messaging-compat.js')

const firebaseConfig = {
  apiKey: "AIzaSyDsDPQjyYVonXFSYeTZ29hiXSkJr4t11MQ",
  authDomain: "test-push-eeebe.firebaseapp.com",
  projectId: "test-push-eeebe",
  storageBucket: "test-push-eeebe.appspot.com",
  messagingSenderId: "236770286911",
  appId: "1:236770286911:web:434e3c132ca327948bc78a",
  measurementId: "G-HHPTB2ZNDW"
};

// Initialize Firebase
const app = firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging(app);

messaging.onBackgroundMessage(payload => {
    console.log('Recibiendo mensaje en segundo plano');
    const tituloNotificacion = payload.notification.title;
    const options = {
        body: payload.notification.body,
        icon: '../img/40.png'
    }

    self.registration.showNotification(tituloNotificacion, options);
})
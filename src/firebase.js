// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getMessaging, getToken } from 'firebase/messaging'

const firebaseConfig = {
  apiKey: "AIzaSyDsDPQjyYVonXFSYeTZ29hiXSkJr4t11MQ",
  authDomain: "test-push-eeebe.firebaseapp.com",
  projectId: "test-push-eeebe",
  storageBucket: "test-push-eeebe.appspot.com",
  messagingSenderId: "236770286911",
  appId: "1:236770286911:web:434e3c132ca327948bc78a",
  measurementId: "G-HHPTB2ZNDW"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);
export const messaging = getMessaging(app);
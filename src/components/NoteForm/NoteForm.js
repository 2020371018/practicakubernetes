import '../../App.css'
import { useEffect, useState } from 'react';
import axios from 'axios';
import { messaging } from '../../firebase'
import { getToken, onMessage } from 'firebase/messaging';
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css'

const NoteForm = () => {

    const getTokenNotification = async () => {
        const token = await getToken(messaging, {
            vapidKey: 'BOvxHiEgpWr9Mh3SgJYwSQ92fSB9nQyjMV1cMhnJf--P-LAciqdfKAs0E1KtgBdoKcgbqb-WSrbp8rj2Bbn0LJw'
        }).catch((err) => console.log('No se pudo obtener el token', err))

        if (token) {
            console.log('Token', token);
        } if (!token) {
            console.log('No hay token disponible')
        }
    }

    const notificarme = () => {
        if (!window.Notification) {
            console.log('Este navegador no soporta notificaciones')
            return; 
        }

        if (Notification.permission === 'granted') {
            getTokenNotification(); // Obtener y mostrar el token en la consola
        } else if (Notification.permission !== 'denied' || Notification.permission === 'default') {
            Notification.requestPermission((permission) => {
                console.log(permission);
                if (permission === 'granted') {
                    getTokenNotification();
                }
            })
        }
    }
    notificarme();

    useEffect(() => {
        getTokenNotification();
        onMessage(messaging, message => {
            console.log('onMessage: ', message);
            toast(message.notification.title)
        })
    }, [])

    const [notes, setNotes] = useState([]);
    const [newNote, setNewNote] = useState({ title: '', text: '' });
  
    useEffect(() => {
      axios.get('http://localhost:3000/api/note')
        .then((response) => setNotes(response.data))
        .catch((error) => console.error('Error fetching notes:', error));
    }, []);
  
    const handleNoteSubmit = () => {
      // Verificar que el título no esté vacío antes de crear una nota
      if (newNote.title.trim() === '') {
        alert('El título no puede estar vacío.');
        return;
      }
      // Verificar que la nota no esté vacía
      if (newNote.text.trim() === '') {
        alert('La nota no puede estar vacía.');
        return;
      }
  
      axios.post('http://localhost:3000/api/note', newNote)
        .then((response) => {
          setNotes([...notes, response.data]);
          setNewNote({ title: '', text: '' });
        })
        .catch((error) => console.error('Error creating note:', error));
    };
  
    return (
      <div className="App">

        <ToastContainer />

        <header className="App-header">
          <form>
            <input
              type="text"
              placeholder="Título"
              value={newNote.title}
              onChange={(e) => setNewNote({ ...newNote, title: e.target.value })}
            />
            <input
              type="text"
              placeholder="Texto de la nota"
              value={newNote.text}
              onChange={(e) => setNewNote({ ...newNote, text: e.target.value })}
            />
            <button type="button" onClick={handleNoteSubmit}>
              Crear nota
            </button>
          </form>
          {notes.length === 0 ? (
            <p>No hay notas disponibles.</p>
          ) : (
            <ul>
              {notes.map((note) => (
                <li key={note._id}>
                  <h3>{note.title}</h3>
                  <p>{note.text}</p>
                </li>
              ))}
            </ul>
          )}
        </header>
      </div>
    );
}

export default NoteForm;
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Login from "../Login/Login";
import NoteForm from "../NoteForm/NoteForm";

const Navigation = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" index element={<Login />} />
                <Route path="/notes" element={<NoteForm />} />
            </Routes>
        </BrowserRouter>
    )
}

export default Navigation;
import { getAuth, signInAnonymously } from 'firebase/auth'
import { useNavigate } from 'react-router-dom'

const Login = () => {

    const navigate = useNavigate()

    const navigateToNotes = () => {
        navigate('/notes')
    }

    const login = () => {
        signInAnonymously(getAuth()).then((usuario) => {
            console.log(usuario)
            navigateToNotes()
        })
    }

    return (
        <div>
            <input type='button' onClick={login} value='Iniciar Sesion' />
        </div>
    )

}

export default Login;